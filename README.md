# Name： Answer of Assignment1: Predictive Modelling of Eating-Out problem

## Overview

There are two parts in this assignment, PartA and PartB, which draw some conclusions based on the real-world dataset provided and the knowledge of feature engineering, modeling and deployment learned in this unit.

## How to Run

1. find the project: git link https://gitlab.com/sususuncarrie/assignment1final.git

2. Download the file and upload it to jupyter to run it.

## Usage

- Enter "Assignment1_PartA_Kylie.ipynb " to check the answer of part A

- Enter "Assignment1_PartB_Kylie.ipynb " to check the answer of part B

## What the user would expect if got the code running

- After successfully running the code, the user should be able to obtain an in-depth understanding of the dataset, including analyses and conclusions about data distribution, model performance, and more. 

- Additionally, users should be able to obtain detailed information about the performance of linear regression and logistic regression models, as well as confusion matrices and observations relevant to the classification task.

## Contribution

- Attempt to complete each detailed task in an organized manner according to the requirements of the assignment.

## Author

- Kylie Sun
- Email: sususuncarrie@gmail.com
- Student ID: u3243870

